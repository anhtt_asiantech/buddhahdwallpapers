package com.anhttvn.buddhahdwallpapers.util;

public class ConfigUtil {
    public static final String FOLDER_DOWNLOAD = "BuddhaHDWallpapers";
    public static final String FOLDER_ASSETS = "wallpaper";
    /**
     * api
     */
    public static String KEY_NOTIFICATION = "Notification";

    public static String KEY_WALLPAPER = "WALLPAPER_V5";
    public static String INFORMATION ="Information";

//  Config Database

    public static  final String DATABASE = "BuddhaHDWallpapers.db";
    public static  final int VERSION = 3;
}
